#!/bin/bash -e

# bunnysign is a dumb take on cowsay, but without a cow... and without saying.

# Default Parameters
CONTENT_WIDTH=40
SIGN_PREFIX="| "
SIGN_SUFFIX=" |"
SIGN_CORNER="|"
SIGN_TOP="-"
SIGN_BOT="-"
HUNTING_SEASON="DUCK"
SPLIT_BY="WORD"

# Bunny Lego®
LEFT_EYE='•'
RIGHT_EYE='•'
MOUTH_NOSE_THING='ㅅ'

# Prints the top/botton part of the sign
printBotTopSign() {
    local CHAR="$1"
    local COUNT="$2"

    # Some magical shit
    echo "${SIGN_CORNER}$(printf %$((COUNT+2))s |tr " " "${CHAR}")${SIGN_CORNER}"
}

# =============================================================================
# Main™
# =============================================================================

# Argument parsing
while [[ ${#} -gt 0 ]]; do
    case "${1}" in
        -d|--dead)
            LEFT_EYE="ˣ"
            RIGHT_EYE="ˣ"
            MOUTH_NOSE_THING="__"
            shift # past argument
            ;;
        -s|--surprised)
            LEFT_EYE=" °"
            RIGHT_EYE="°"
            MOUTH_NOSE_THING="o"
            shift # past argument
            ;;
        -w|--width)
            CONTENT_WIDTH="$2"
            FIXED_WIDTH="true"
            shift # past argument
            shift # past value
            ;;
        -n|--no-bunny|--bunnyless)
            HUNTING_SEASON="BUNNY"
            shift # past argument
            ;;
        --split-by-word)
            SPLIT_BY="WORD"
            shift # past argument
            ;;
        *) # unknown option
            if [ -z "${MESSAGE}" ]; then
                # If the message isn't defined
                # start a new message
                MESSAGE="${1}"
            else
                # If the message is defined
                # concatenate the new content
                MESSAGE+=" ${1}"
            fi
            shift # past argument
            ;;
    esac
done

# Adapt Sign size to content if message is small enough
[ -z "${FIXED_WIDTH}" ] && [ ${#MESSAGE} -lt ${CONTENT_WIDTH} ] && CONTENT_WIDTH=${#MESSAGE}

if [ "${SPLIT_BY}" == "WORD" ]; then
    MESSAGE_LINES=(${MESSAGE})
    if [ -z "${FIXED_WIDTH}" ]; then
        CONTENT_WIDTH=1
        for LINE in ${MESSAGE_LINES[*]}; do
            [ ${#LINE} -gt ${CONTENT_WIDTH} ] && CONTENT_WIDTH=${#LINE}
        done
    fi
fi

# TOP SIGN 🔝👌
printBotTopSign ${SIGN_TOP} ${CONTENT_WIDTH}

# MESSAGE
for LINE in ${MESSAGE_LINES[*]}; do
    LINE_LEN=${#LINE}
    PADDING_LEN=$((CONTENT_WIDTH-LINE_LEN))
    PADDING="$(printf %${PADDING_LEN}s)"
    echo "${SIGN_PREFIX}${LINE}${PADDING}${SIGN_SUFFIX}"
done


# BOTTON SIGN
printBotTopSign ${SIGN_BOT} ${CONTENT_WIDTH}

# The Bunny, finally
if [ "${HUNTING_SEASON}" != "BUNNY" ]; then
    BUNNY="(\\__/) ||\n(${LEFT_EYE}${MOUTH_NOSE_THING}${RIGHT_EYE}) ||\n/ 　 づ\n"
fi

echo -e -n ${BUNNY}